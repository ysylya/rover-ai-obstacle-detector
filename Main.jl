# import Pkg; 
# Pkg.add("ImageView")
# Pkg.add("Images")
# Pkg.add("FStrings")
# Pkg.add("ImageMagick")
# Pkg.add("ImageMorphology")
# Pkg.add("Statistics")


module peti

    using ImageView
    using Images
    using FStrings
    using ImageMagick
    using ImageMorphology
    using Statistics
    using FileIO

    CHUNK_PX = 32
    IMGNAME_PADDING_IN_BLEXPORTS = 3
    IMGNAME_PADDING_IN_CHUNKEDS = 6 # TODO calc from amount

    # const ImgPath = String

    function log_info(data...)
        open("log.txt", "a") do file_log
            write(file_log, "INFO\t", string(data...), "\n")
        end
    end


    function log_tsv_line(line)
        open("log.tsv", "a") do file_tsv
            write(file_tsv, line, "\n")
        end
    end


    function display_in_column(imgs...)
        # @show typeof(imgs)
        # @show length(imgs)

        count = length(imgs)

        if count < 1
            return nothing
        end

        height, width = size(imgs[1])

        combined = fill(RGB{Float64}(0.0, 0.0, 0.0), (height * count, width))
        # combined = fill(RGB4{Float64}(0.,0.,0.), size(img1) .* (1, 2))
        for i in 1:count
            # combined[:, (width*(i-1))+1:width*i] = imgs[i]
            # combined[(height*(i-1))+1:height*i, :] = imgs[i]
            combined[(height*(i-1))+1:height*i, :] = imgs[i]
        end

        display(combined)

    end

    function preprocess_image(img)
        # @show summary(img)
        img_gray = Gray.(img)
        img_binary = (1 * img_gray .> 0.35) .* -1 .+1

        # display_in_column(opening((erode(img_binary))), img_binary, img_gray, img)

        return img_gray
    end

    function generate_chunks_from_one_img(img_train, img_validation, path_chunk_folder, 
                                          imgname_padding_in_outputs, img_nth, chunk_px, 
                                          file_nn_train_y)
        
        steps = size(img_train)[1] ÷ chunk_px
        # imshow(img_train)
        # imshow(img_validation)
        for h in 1:steps, w in 1:steps
            h_min = chunk_px * (h-1) + 1
            h_max = chunk_px * h
            w_min = chunk_px * (w-1) + 1
            w_max = chunk_px * w
            # chunk = Gray.(img[h_min:h_max, w_min:w_max])
            chunk_train = (img_train[h_min:h_max, w_min:w_max])
            chunk_validation = (img_validation[h_min:h_max, w_min:w_max])

            numbering = lpad(((img_nth-1)*(steps^2)) + (h-1)*steps+w, imgname_padding_in_outputs, '0')
            save(joinpath(path_chunk_folder, string("chunk_", numbering, ".png")), chunk_train)

            num_of_whites = 0
            for px in chunk_validation
                if px > 0.01
                # if px.r > 0.01
                    num_of_whites += 1
                end
            end
            # @show num_of_whites
            # only consider it a rock pic if at least 10% of the pic is a part of a rock
            write(file_nn_train_y, string(num_of_whites/CHUNK_PX^2 >= 0.1 ? 1 : 0, "\n"))
        end
    end


    function generate_chunks_from_n_img(path_train_folder, path_chunk_folder, img_amount, chunk_px)
        path_nn_train_y = mkpath(path_chunk_folder)
        open(joinpath(path_chunk_folder, "nn_train_y.txt"), "w") do file_nn_train_y
            for i in 1:img_amount
                numbering = lpad(i, IMGNAME_PADDING_IN_BLEXPORTS, '0')
                img_train = load(joinpath(path_train_folder, string("train_input_", numbering, ".png")))
                img_train = preprocess_image(img_train)
                img_validation = Gray.(load(joinpath(path_train_folder, string("train_validation_", numbering, ".png"))))
                generate_chunks_from_one_img(img_train, img_validation, path_chunk_folder,
                                             IMGNAME_PADDING_IN_CHUNKEDS, i, chunk_px,
                                             file_nn_train_y)
            end
        end
    end


    sigmoid(x::Float64) = clamp(one(x) / (one(x) + exp(-x)), 0.0+eps(x), 1.0-eps(x))
    # sigmoid_prime(x) = s = sigmoid(x); s * (one(x) - s)
    function sigmoid_prime(x::Float64)
        s = sigmoid(x)
        return s * (one(x) - s)
    end

    relu(x) = max(0, x)
    relu_prime(x) = (x >= 0.0) ? 1.0 : 0.0


    mutable struct NeuralNet
        w::Array{Matrix{Float64}}
        b::Array{Vector{Float64}}
        activate::Array{Function,1}
        activate_prime::Array{Function,1}
        num_layers::UInt

        function NeuralNet(neurons_per_layers::Tuple)
            # @show neurons_per_layers
            # w = Array[]
            # w = Array{Array{Float64, 2}}(undef, 0)
            w = Array{Matrix{Float64}}(undef, 0)
            b = Array{Vector{Float64}}(undef, 0)
            activate = Function[]
            activate_prime = Function[]
            for i in 2:length(neurons_per_layers)
                # dim: n unit on layer * m weights for inputs
                w_on_layer_i::Matrix{Float64} = randn(neurons_per_layers[i], neurons_per_layers[i-1]) .* 0.01
                # dim: n unit on layer
                b_on_layer_i::Vector{Float64} = randn(neurons_per_layers[i]) .* 0.01
                # @show summary(w_on_layer_i)
                # @show summary(b_on_layer_i)
                push!(w, w_on_layer_i)
                push!(b, b_on_layer_i)

                # push!(activate, (i < length(neurons_per_layers) ? relu : sigmoid))
                push!(activate, sigmoid)
                # push!(activate_prime, (i < length(neurons_per_layers) ? relu_prime : sigmoid_prime))
                push!(activate_prime, sigmoid_prime)
            end
            
            # for i in 1:size(w, 1)
            #     @show i
            #     @show summary(w[i])
            #     display(w[i])
            #     @show summary(b[i])
            #     display(b[i])
            # end
            # @show summary(w)
            # display(w)
            # @show summary(b)
            # display(b)

            new(w, b, activate, activate_prime, size(neurons_per_layers, 1) - 1)
        end
    end

    function Base.summary(nn::NeuralNet)
        return string("NeuralNet |layers: ", nn.num_layers, "| |weights: ", summary(nn.w), "| |biases: ", summary(nn.b), "|")
    end


    function FileIO.save(nn::NeuralNet, path_to_location_dir::String, filename::String)
        mkpath(path_to_location_dir)
        open(joinpath(path_to_location_dir, filename), "w") do file_nn
            write(file_nn, "weights\n")
            write(file_nn, string(nn.w), "\n")
            write(file_nn, "biases\n")
            write(file_nn, string(nn.b), "\n")
            write(file_nn, "activate\n")
            write(file_nn, string(nn.activate), "\n")
            write(file_nn, "activate_prime\n")
            write(file_nn, string(nn.activate_prime), "\n")
            write(file_nn, "num_layers\n")
            write(file_nn, string(nn.num_layers), "\n")
        end
    end


    function get_img_as_dim1_array(img_path::String)
        img = load(img_path)
        # ImageMagick.display(img)
        # @show summary(img)
        # @show size(img, 1)*size(img, 2)
        arr_x = reshape(img, size(img, 1)*size(img, 2))
        # @show summary(arr_x)
        return convert(Array{Float64}, arr_x)
    end


    function get_ith_example(path_train_chunk::String, i)
        # function load_nth_img(img_path_input::String, n::Signed)
        numbering = lpad(i, IMGNAME_PADDING_IN_CHUNKEDS, '0')
        img_path_input = joinpath(path_train_chunk, string("chunk_", numbering, ".png"))
        x = get_img_as_dim1_array(img_path_input)

        y = -1.0
        open(joinpath(path_train_chunk, "nn_train_y.txt"), "r") do file_nn_train_y
            for i in 1:i-1
                # println("l_", i, ": ", readline(file_nn_train_y))
                readline(file_nn_train_y)
            end
            y = parse(Float64, readline(file_nn_train_y))
            # println("yyy ", y)
        end

        return x, y
    end

    function get_examples(path_train_chunk::String, num_of_examples::Signed)
        X = Array{Array{Float64}}(undef, 0)
        # @show summary(X)
        Y = Array{Float64}(undef, 0)

        open(joinpath(path_train_chunk, "nn_train_y.txt"), "r") do file_nn_train_y
            for i in 1:num_of_examples
                numbering = lpad(i, IMGNAME_PADDING_IN_CHUNKEDS, '0')
                img_path_input = joinpath(path_train_chunk, string("chunk_", numbering, ".png"))
                x_i = get_img_as_dim1_array(img_path_input)
                # @show summary(x_i)
                push!(X, x_i)

                y_i = parse(Float64, readline(file_nn_train_y))
                push!(Y, y_i)
            end
        end
        # @show summary(X)
        X = hcat(X...) # array of array to matrix
        # @show summary(X)

        # @show summary(Y)
        Y = hcat(Y...) # row vector to column vector
        # @show summary(Y)
        return X, Y
    end


    function feedforward(nn::NeuralNet, X::Matrix)
        # A::Array{Matrix} = X
        # A = Array{Matrix{Float64}}(undef, 0)

        Z::Array{Matrix{Float64}} = []
        A::Array{Matrix{Float64}} = []

        A_prev::Matrix{Float64} = X

        # println("A_prev")
        # display(A_prev)

        for ith_layer in 1:nn.num_layers
            W_i::Matrix = nn.w[ith_layer]
            b_i::Vector = nn.b[ith_layer]

            Z_i::Matrix = W_i * A_prev .+ b_i
            push!(Z, Z_i)

            A_i::Matrix = nn.activate[ith_layer].(Z_i)
            push!(A, A_i)

            A_prev = A_i
        end

        return A_prev, Z, A
        
    end


    # TODO NaN with ReLU... (y_hat could be zero)
    function loss(y_hat_i, y_i)
        return -(y_i*log(y_hat_i) + (1-y_i)*log(1-y_hat_i))
    end

    
    function backprop!(nn::NeuralNet, Z::Array{Matrix{Float64}}, A::Array{Matrix{Float64}}, 
                       X::Matrix{Float64}, Y::Matrix{Float64},
                       num_of_examples::Signed, learning_rate::Float64)

        dA_l = (.-Y ./ A[nn.num_layers]) .+ (1.0 .- Y) ./ (1.0 .- A[nn.num_layers])

        for l in nn.num_layers:-1:1
            dZ_l = dA_l .* nn.activate_prime[l].(Z[l])
            dW_l = (1.0 / num_of_examples) * (dZ_l * transpose( (l > 1) ? A[l-1] : X ))
            db_l = (1.0 / num_of_examples) * sum(dZ_l, dims=2)

            nn.w[l] = nn.w[l] - learning_rate .* dW_l
            nn.b[l] = nn.b[l] - learning_rate .* vec(db_l) 

            dA_l = transpose(nn.w[l]) * dZ_l
        end
    end


    function train!(nn::NeuralNet, path_train_chunk::String, num_of_examples::Signed, 
                    learning_rate::Float64 = 0.001, iters::Signed = 1000)
        alpha::Float64 = learning_rate

        # dimX: n input units * m examples
        # dimY: m examples
        X::Matrix{Float64}, Y::Matrix{Float64} = get_examples(path_train_chunk, num_of_examples) # TODO Ys should be matrices
        # X::Matrix, Y::Vector = (randn(3, 2), randn(2))
        # @show summary(X)
        # @show summary(Y)

        Y_HAT::Matrix{Float64}, Z::Array{Matrix{Float64}}, A::Array{Matrix{Float64}} = feedforward(nn, X)
        # @show summary(Y_HAT)

        cost_start::Float64 = (1.0 / num_of_examples) * sum(loss.(Y_HAT, Y))

        # for i in 1:size(Y_HAT, 1)
        #     @show Y_HAT[i]
        #     @show Y[i]
        #     @show loss(Y_HAT[i], Y[i])
        # end
        # @show sum(loss.(Y_HAT, Y))
        # if (sum(loss.(Y_HAT, Y)) === NaN)
        #     println("nan")
        #     @show cost_start
        # end
        println("backprop started")
        prev_print = 0
        for i in 1:iters
            Y_HAT, Z, A = feedforward(nn, X)
            backprop!(nn, Z, A, X, Y, num_of_examples, alpha)
            # cost::Float64 = (1.0 / num_of_examples) * sum(loss.(Y_HAT, Y))
            percent = i / iters * 100
            # println(percent, "%")
            if (percent >= prev_print + 10)
                println(percent, "%")
                prev_print = percent
            end
        end
        println("backprop finished")

        Y_HAT, Z, A = feedforward(nn, X)
        cost_end::Float64 = (1.0 / num_of_examples) * sum(loss.(Y_HAT, Y))

        result = string("cost_start: ", cost_start)
        log_info(result)
        println(result)

        result = string("cost_end: ", cost_end)
        log_info(result)
        println(result)

        return cost_start, cost_end

        # result = string("found ", count(==(1), Y_HAT), "/", count(==(1), Y))
        # log_info(result)
        # println(result)
            
    end


    function guess_chunk(nn::NeuralNet, X::Matrix)
        Y_HAT::Matrix{Float64}, Z::Array{Matrix{Float64}}, A::Array{Matrix{Float64}} = feedforward(nn, X)
        return Y_HAT[1] > 0.5
    end


    function guess_chunk(nn::NeuralNet, img_path::String)
        x = get_img_as_dim1_array(img_path)
        return guess_chunk(nn, x)
    end


    function guess_full(nn::NeuralNet, img_path::String, chunk_px::Signed)
        img = Gray.(load(img_path))
        steps = size(img)[1] ÷ chunk_px
        # @show steps
        # imshow(Gray.(img))

        y_hat = falses(steps, steps)
        for h in 1:steps
            for w in 1:steps
                h_min = chunk_px * (h-1)+1
                h_max = chunk_px * h
                w_min = chunk_px * (w-1)+1
                w_max = chunk_px * w
                
                chunk = img[h_min:h_max, w_min:w_max]
                # @show summary(chunk)
                X::Matrix = reshape(chunk, (chunk_px*chunk_px, 1))
                # @show summary(X)
                X = convert(Array{Float64}, X)
                # @show summary(X)
                y_hat[h, w] = guess_chunk(nn, X)
            end
        end
        # @show y_hat
        # show(stdout, "text/plain", y_hat)
        # println()
        # printstyled(y_hat)
        return y_hat
    end

    
    function convert_bitarray_to_rgba_array(to_be_converted::BitArray{2}, 
                                            color::RGBA{N0f8}, scaleup_factor::Signed)
        sizes = (size(to_be_converted)[1]*scaleup_factor, size(to_be_converted)[2]*scaleup_factor)
        arr_rgba = fill(RGBA{N0f8}(0.0, 0.0, 0.0, 0.0), sizes)
        
        for h in 1:size(arr_rgba)[1], w in 1:size(arr_rgba)[2]
            if to_be_converted[(h-1) ÷ scaleup_factor + 1, (w-1) ÷ scaleup_factor + 1] == true
                arr_rgba[h, w] = color
            # else
            #     arr_rgba[h, w] = color
            end
        end

        # @show summary(convert(Array{RGBA{N0f8}, 2}, to_be_converted * color))
        return arr_rgba

        # @show convert(Array{RGBA{N0f8}, 2}, to_be_converted * color)
    end

    # function plot_guess(img_path::String, guess::AbstractArray{Bool})
    function plot_guess(img_path::String, guess::BitArray{2})
        img = load(img_path)
        # @show summary(img)

        # steps = size(img)[1] ÷ size(guess)[1]
        # overlay = convert_bitarray_to_rgba_array(guess, RGBA{N0f8}(0.0, 1.0, 0.0, 0.5), steps)

        # imshow(overlay)

        # img_overlay = convert(Array{N0f8}, guess)

        color = RGBA{N0f8}(0.0, 1.0, 0.0, 0.5)
        scaleup_factor = size(img)[1] ÷ size(guess)[1]
        sizes = (size(guess)[1]*scaleup_factor, size(guess)[2]*scaleup_factor)
        arr_rgba = fill(RGBA{N0f8}(0.0, 0.0, 0.0, 0.0), sizes)
        
        for h in 1:size(arr_rgba)[1], w in 1:size(arr_rgba)[2]
            if guess[(h-1) ÷ scaleup_factor + 1, (w-1) ÷ scaleup_factor + 1] == true
                color_new = 0.75 .* img[h, w] + color
                color_new = RGBA{N0f8}(
                                clamp(red(color_new), 0.0, 1.0),
                                clamp(green(color_new), 0.0, 1.0),
                                clamp(blue(color_new), 0.0, 1.0),
                                clamp(alpha(color_new), 0.0, 1.0)

                )
                arr_rgba[h, w] = color_new
            else
                arr_rgba[h, w] = img[h, w]
            end
        end

        # imshow(arr_rgba)
        return arr_rgba
    end

    function train_and_guess!(nn::NeuralNet, rounds::Integer, path_train_chunk::String, path_test::String,
                              num_of_examples::Integer, learning_rate::Float64, iters::Integer, 
                              ith_attenpt::Integer = 1, total_attempts::Integer = 1)
        
        cost_start::Float64 = -1.0
        costs = Float64[]
        durations = Float64[]

        for round in 1:rounds
            println("round ", round , "/", rounds, " started")
            log_info("round ", round , "/", rounds, " started")
            # for _ in 1:20
            #     @time train2!(nn, path_train_chunk, 1000)
            # end
            train_result = @timed train!(nn, path_train_chunk, num_of_examples, learning_rate, iters)
            time_elapsed = train_result.time
            @show time_elapsed
            log_info("round took ", time_elapsed, " secs == ", time_elapsed/60.0, " mins == ", time_elapsed/3600.0, " hours")

            if round == 1
                cost_start = train_result.value[1]
            end
            push!(costs, train_result.value[2])
            push!(durations, time_elapsed)

            num_of_guess_images = 10
            for i in 1:num_of_guess_images
                numbering = lpad(i, IMGNAME_PADDING_IN_BLEXPORTS, '0')
                imgpath_test = joinpath(path_test, string("test_input_", numbering, ".png"))

                imgpath_guessed_folder = mkpath(joinpath(path_test, "guesses", string("guess_", lpad(ith_attenpt, floor(Integer, log(10, total_attempts)) + 1, '0')))) 
                imgpath_guessed = joinpath(imgpath_guessed_folder, string("test_input_", numbering, "_guess_", round, ".png"))

                guess::BitArray{2} = guess_full(nn, imgpath_test, CHUNK_PX)

                img_guessed = plot_guess(imgpath_test, guess)

                save(imgpath_guessed, img_guessed)
            end

            # println("---> ", round/rounds*100, "%");
            println("round ", round , "/", rounds, " finished");
            log_info("round ", round , "/", rounds, " finished");
            log_info("---")
        end

        return cost_start, costs, durations
    end


    function try_nns(path_train_chunk::String)
        layers::Array{Tuple} = [
            # (CHUNK_PX^2, 128, 64, 64, 1)
            # (CHUNK_PX^2, 64, 64, 1)
            (CHUNK_PX^2, 32, 32, 1)
            (CHUNK_PX^2, 32, 1)
            (CHUNK_PX^2, 1)
        ]

        learning_rates::Array{Float64} = [
            0.1
            0.25
            0.5
            1.0
            # 0.01
            # 0.001
            # 0.0001
        ]

        iters::Array{Integer} = [
             100
            #  200
            #  500
            1000
            # 2000
        ]

        rounds::Integer = 20

        tsv_header::String = "attempt\tlayer_structure\tlearning_rate\titer_per_round\ttotal_duration_mins\tstart_cost\t"
        for i in 1:rounds
            tsv_header *= string("round_", lpad(i, floor(Integer, log(10, rounds)) + 1, '0'), "_cost\t")
        end
        for i in 1:rounds
            tsv_header *= string("round_", lpad(i, floor(Integer, log(10, rounds)) + 1, '0'), "_duration_mins\t")
        end
        log_tsv_line(tsv_header)

        total_attempts = size(layers, 1) * size(learning_rates, 1) * size(iters, 1)

        i = 1
        for layer in layers, learning_rate in learning_rates, iter in iters
            log_info("-----------> NN start")
            log_info(i, "/", total_attempts, " attempt")
            log_info("layer: ", layer)
            log_info("learning_rate: ", learning_rate)
            log_info("iter: ", iter)
            log_info("---")

            nn1 = NeuralNet(layer)
            train_and_guess_result = @timed train_and_guess!(nn1, rounds, path_train_chunk, joinpath("nn_data", "test"), 19200, learning_rate, iter, i, total_attempts)
            time_elapsed = train_and_guess_result.time
            
            @show time_elapsed
            log_info("training took ", time_elapsed, " secs == ", time_elapsed/60.0, " mins == ", time_elapsed/3600.0, " hours")
            log_info("---")
            log_info("-----------> NN end")

            @show train_and_guess_result.value
            total_duration_mins = sum(train_and_guess_result.value[3]) / 60.0
            @show total_duration_mins
            tsv::String = string(i, "\t", layer, "\t", learning_rate, "\t", iter, "\t", 
                                 total_duration_mins, "\t", train_and_guess_result.value[1], "\t")
            for i in 1:rounds
                tsv *= string(train_and_guess_result.value[2][i], "\t")
            end
            for i in 1:rounds
                tsv *= string(train_and_guess_result.value[3][i] / 60.0, "\t")
            end
            log_tsv_line(tsv)

            save(nn1, "saved_nns", string("NeuralNet_", lpad(i, floor(Integer, log(10, total_attempts)) + 1, '0')))
            
            i += 1
        end
    end


    function main()
        println("******")
        log_info("******")

        # settings = Settings(32, "train/train_", 3, "chunk/chunk_", 4, ".png")

        path_train = joinpath("nn_data", "train")
        path_train_chunk = joinpath(path_train, "chunk")

        generate_chunks_from_n_img(path_train, path_train_chunk, 300, CHUNK_PX)

        # nn1 = NeuralNet((CHUNK_PX^2, 1))
        # @time train_and_guess!(nn1, 20, path_train_chunk, joinpath("nn_data", "test"), 640, 0.001, 200)

        try_nns(path_train_chunk)

        println("******")
        log_info("******")
    end

end


peti.main()
