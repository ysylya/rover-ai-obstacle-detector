# Martian rover AI simulation
#### _Part 2/2: Obstacle detector_

This project simulates a martian rover detecting obstacles (rocks) on the surface.
The simulation has two parts:

* 1/2: [Surface image generator](https://gitlab.com/ysylya/rover-ai-surface-image-generator)
* 2/2: Obstacle detector

This is Part 2, where I implemented a neural network from scratch (without any external ML library).

There is a simple image generator blend file included for quick testing purposes (much simpler than the other project).

The neural network is a kind of a binary classificator, so after teaching it, it can tell whether a rock is present on the picture or not.
In order to overcome the obvious limitation (where is the rock exactly), firstly, the image is being split into smaller chunks, the model detects whether there is a rock (part) on it or not, colors the image, then the chunks are being recombined.

For teaching, there needs to be a normal and a BW coded image available from the scene (more details in the 1. project). It learns whether there is a white colorcoded object (part) on the normal image.

This is an experimenting project, it is very easy to set and test different

* layer architectures (depth, width for each layer)
* learning rates
* iterations

in one run.

As the example pictures show, the algorithm works quite well (each image shows a detection with different hyperparameters).

![01](img/01.png)

![02](img/02.png)

![03](img/03.png)

![04](img/04.png)

